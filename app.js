const express = require('express');  
const bodyParser = require('body-parser');
const cors=require('cors');
const app = express();


app.use(bodyParser.json() );                         // to support JSON-encoded bodies like from postman sending the raw data//
app.use(bodyParser.urlencoded({extended: true }));   // to support URL-encoded bodies  x-www-form-url-encode like simple html form without enctype//

app.use(cors())

const emps = [
				{
			        id: 1,
			        name: 'Manish ku',
			        mobno: '0123456789'
    			},
			    {
			        id: 2,
			        name: 'Kumar',
			        mobno: '9876543210'
			    },
			    {
			        id: 3,
			        name: 'Gupta',
			        mobno: '0123459876'
			    }
];

app.get("/",function(req, res){
    res.status(200).send({ "confirmed": 0, "message":"App is running successfully"});
});


app.get('/emp', function(req, res){
	res.status(200).send({ "confirmed": 0, "data":emps });
});



const port = process.env.port || 3000;
app.listen(port,function(req, res){
    console.log(`Server is running at port no ${port}`);
});

/*
	To run the app, open the terminal in root path off application and hit the command.
	install nodemon globally or locally
	nodemon index.js
*/
